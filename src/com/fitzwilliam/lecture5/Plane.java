package com.fitzwilliam.lecture5;

/**
 * @author alisovenko
 * 10/27/16.
 */
// Exercise Plane.java
// Program reserves airline seats.

import java.util.Scanner;

public class Plane {
    // checks customers in and assigns them a boarding pass
    public void checkIn() {
        Scanner input = new Scanner(System.in);

        boolean seats[] = new boolean[12]; // array of seats
        int firstClass = 0; // next available first class seat
        int economy = 6; // next available economy seat

        while ((firstClass < 6) || (economy < 12)) {
            System.out.println("Please type 1 for First Class");
            System.out.println("Please type 2 for Economy");
            System.out.print("choice: ");
            int section = input.nextInt();

            if (section == 1) // user chose first class
            {
                if (firstClass < 6) {
                    seats[firstClass] = true;
                    firstClass++;
                    System.out.printf("First Class. Seat #%d\n", firstClass);
                } // end if
                else if (economy < 12) // first class is full
                {
                    System.out.println(
                            "First Class is full, Economy Class?");
                    System.out.print("1. Yes, 2. No. Your choice: ");
                    int choice = input.nextInt();

                    if (choice == 1) {
                        seats[economy] = true;
                        economy++;
                        System.out.printf("Economy Class. Seat #%d\n",
                                economy);
                    } else
                        System.out.println("Next flight leaves in 3 hours.");
                } // end else if
            } // end if
            else if (section == 2) // user chose economy
            {
                if (economy < 12) {
                    seats[economy] = true;
                    economy++;
                    System.out.printf("Economy Class. Seat #%d\n", economy);
                } // end if
                else if (firstClass < 6) // economy class is full
                {
                    System.out.println(
                            "Economy Class is full, First Class?");
                    System.out.print("1. Yes, 2. No. Your choice: ");
                    int choice = input.nextInt();

                    if (choice == 1) {
                        firstClass++;
                        System.out.printf("First Class. Seat #%d\n",
                                firstClass);
                    } // end if
                    else
                        System.out.println("Next flight leaves in 3 hours.");
                } // end else if
            } // end else if
            else if (section == 3) {
                printSeats(seats);
            }

            System.out.println();
        } // end while

        System.out.println("The plane is now full.");
    } // end method checkIn

    private void printSeats(boolean[] seats) {
        for (int i = 0; i < seats.length; i++) {
            if (i % 2 == 0)
                System.out.println();

            if (i == 0) {
                System.out.println("First-class");
                System.out.println();
            } else if (i == 6) {
                System.out.println("Economy");
                System.out.println();
            }

            if (seats[i])
                System.out.print('*');
            else
                System.out.print('_');
        }
    }
} // end class Plane














