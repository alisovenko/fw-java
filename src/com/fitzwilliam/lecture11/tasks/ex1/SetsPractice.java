package com.fitzwilliam.lecture11.tasks.ex1;

import java.util.HashSet;
import java.util.Set;

/**
 * @author alisovenko
 *         11/22/16.
 */
public class SetsPractice {
    private static final String colors[] = {"black", "yellow",
            "green", "blue", "black", "violet", "silver", "yellow"};

    public static void main(String[] args) {
        Set<String> set = new HashSet<>();

        int cnt = 0;
        for (String color : colors) {
            // the following is the same as
            /*boolean added = set.add(color);
            if (!added) {
                System.out.println("Duplicate: " + color);
                cnt++;
            }*/
            if (!set.add(color)) {
                System.out.println("Duplicate: " + color);
                cnt++;
            }
        }

        if (set.contains("silver"))
            System.out.println("Contains silver");

        if (set.contains("cyan"))
            System.out.println("Contains cyan");

        set.remove("violet");

        System.out.println("Number of duplicates " + cnt + ", set: " + set);
    }
}
