package com.fitzwilliam.lecture6;

/**
 * @author alisovenko
 *         11/3/16.
 */
public class BasePlusCommissionEmployee2 extends CommissionEmployee2 {
    private double baseSalary;

    public BasePlusCommissionEmployee2(String firstName, String lastName, String socialSecurityNumber, double grossSales, double commissionRate, double baseSalary) {
        super(firstName, lastName, socialSecurityNumber, grossSales, commissionRate);
        this.baseSalary = baseSalary;
    }

    public double getBaseSalary() {
        return baseSalary;
    }

    public void setBaseSalary(double baseSalary) {
        this.baseSalary = baseSalary;
    }

    public double earning() {
        return baseSalary + (getCommissionRate() * getGrossSales());
    }

}
