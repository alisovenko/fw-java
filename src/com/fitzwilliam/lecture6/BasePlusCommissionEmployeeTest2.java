package com.fitzwilliam.lecture6;

/**
 * @author alisovenko
 *         11/1/16.
 */
public class BasePlusCommissionEmployeeTest2 {
    public static void main(String[] args) {
        BasePlusCommissionEmployee2 employee = new BasePlusCommissionEmployee2("Bob", "Lewis", "333-33-3333", 5000, .04, 300);

        System.out.println("Before base salary update:");
        System.out.println(employee.toString());
        System.out.println(employee.getFirstName());
        System.out.println(employee.earning());

        employee.setBaseSalary(1000);
        System.out.println("After base salary update:");
        System.out.println(employee.toString());
        System.out.println(employee.earning());
    }
}
