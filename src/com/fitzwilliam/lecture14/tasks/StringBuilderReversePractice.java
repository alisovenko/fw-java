package com.fitzwilliam.lecture14.tasks;

/**
 * Your task is to reverse the String. Try to use StringBuilder and String.charAt() method for this.
 * <p>
 * reverse("Hallo") -> "ollaH"
 * reverse("baaaad") -> "daaaab"
 * reverse("aaaa") -> "aaaa"
 * reverse("") -> ""
 *
 * @author alisovenko
 *         12/1/16.
 */
public class StringBuilderReversePractice {
    private static String reverse(String s) {
        // Return "" if string is null
        // 1. create new StringBuilder object.
        // 2. iterate in the counting loop from the end of string (s.length() - 1) to 0. Use i-- for looping in reverse direction
        // 3. in the loop use StringBuilder.append() method to append the character at the i position (use String.charAt(i) for it)
        // 4. Return String (use toString() method of StringBuilder)
        return null;
    }

    public static void main(String[] args) {
        checkStrings("Hallo", "ollaH");
        checkStrings("abcd", "dcba");
        checkStrings("Abcd", "dcbA");
        checkStrings("aaaa", "aaaa");
        checkStrings("baaaad", "daaaab");
        checkStrings("a", "a");
        checkStrings("", "");
        checkStrings(null, "");
    }

    private static void checkStrings(String in, String expected) {
        try {
            String res = reverse(in);
            if (!res.equals(expected))
                System.err.printf("Expected \"%s\", got \"%s\"\n", expected, res);
            else
                System.out.printf("\"%s\" => \"%s\", Correct!\n", in, expected);
        } catch (Exception e) {
            System.err.printf("Expected \"%s\" for \"%s\", got the error \"%s\"\n", expected, in, e.getMessage());
        }

    }
}
