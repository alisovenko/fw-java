package com.fitzwilliam.lecture14.tasks;

/**
 * Your task is to split the string to array of strings with delimiter '|' and concatenate it back using ','
 * Use String.split() method for splitting and StringBuilder for joining.
 *
 * @author alisovenko
 *         12/1/16.
 */
public class StringSplittingPractice {
    private static String replaceDelimiter(String s) {
        // Return "" if string is null
        // 1. split the s parameter to String[] tokens. Use split("-") method for this
        // 2. create StringBuilder object
        // 3. iterate through the tokens array using counting loop
        // 4. append the String token to StringBuilder object using "append()" method.
        // 5. if the token is not the last one (if (i != tokens.length - 1)) - append ',' to the StringBuilder.
        // 6. Return the String from StringBuilder using "toString()" method of the last
        return null;
    }

    public static void main(String[] args) {
        checkStrings("foo-bar", "foo,bar");
        checkStrings("apache", "apache");
        checkStrings("x--y--z", "x,,y,,z");
        checkStrings("-", "");
        checkStrings("", "");
        checkStrings(null, "");
    }

    private static void checkStrings(String in, String expected) {
        try {
            String res = replaceDelimiter(in);
            if (!res.equals(expected))
                System.err.printf("Expected \"%s\", got \"%s\"\n", expected, res);
            else
                System.out.printf("\"%s\" => \"%s\", Correct!\n", in, expected);
        } catch (Exception e) {
            System.err.printf("Expected \"%s\" for \"%s\", got the error \"%s\"\n", expected, in, e.getMessage());
        }

    }
}
