package com.fitzwilliam.lecture14.tasks;

/**
 * Given a string return the same string with all 'a' letters replaced with 'z'.
 * This can be done several ways, try the way using char[] array (String.toCharArray())
 * <p>
 * replaceChar("Hallo", 'a', 'z')  -> "Hzllo"
 * replaceChar("Abcd", 'a', 'z')  -> "Abcd"
 * replaceChar("aaaa", 'a', 'z')  -> "aaaa"
 * replaceChar(null, 'a', 'z')  -> ""
 *
 * @author alisovenko
 *         12/1/16.
 */
public class CharArraysPractice {
    private static String replaceChar(String target, char from, char to) {
        // Return "" if string is null
        // 1. get char[] array from target argument using "toCharArray()" method
        // 2. iterate through char[] array using counting loop
        // 3. in the loop check the character on position i - if it is equal to "from" character - replace the array element with "to" character.
        // Otherwise do nothing
        // 4. Return new String from char[] array (use new String(char[] c) constructor for this)
        return null;
    }

    public static void main(String[] args) {
        checkStrings("Hallo", "Hzllo");
        checkStrings("abcd", "zbcd");
        checkStrings("Abcd", "Abcd");
        checkStrings("aaaa", "zzzz");
        checkStrings("bye", "bye");
        checkStrings("", "");
        checkStrings(null, "");
    }

    private static void checkStrings(String in, String expected) {
        try {
            String res = replaceChar(in, 'a', 'z');
            if (!res.equals(expected))
                System.err.printf("Expected \"%s\", got \"%s\"\n", expected, res);
            else
                System.out.printf("\"%s\" => \"%s\", Correct!\n", in, expected);
        } catch (Exception e) {
            System.err.printf("Expected \"%s\" for \"%s\", got the error \"%s\"\n", expected, in, e.getMessage());
        }

    }
}
