package com.fitzwilliam.lecture14.tasks;

/**
 * Given a string, return the string made of its first two chars, so the String "Hello" yields "He". If the string is shorter than length 2, return
 * whatever there is, so "X" yields "X", and the empty string "" yields the empty string "". Note that str.length() returns the length of a string.
 * <p>
 * firstTwo("Hello") → "He"
 * firstTwo("abcdefg") → "ab"
 * firstTwo("ab") → "ab"
 *
 * @author alisovenko
 *         12/1/16.
 */
public class SubstringPracticeTwoFirst {
    public static String firstTwo(String str) {
        // Return "" if string is null
        // if the string length is greater or equal to 2 - return substring() from it, otherwise - just str
        return null;
    }

    public static void main(String[] args) {
        checkStrings("Hello", "He");
        checkStrings("abcd", "ab");
        checkStrings("ab", "ab");
        checkStrings("a", "a");
        checkStrings("", "");
        checkStrings(null, "");
    }

    private static void checkStrings(String in, String expected) {
        try {
            String res = firstTwo(in);
            if (!res.equals(expected))
                System.err.printf("Expected \"%s\", got \"%s\"\n", expected, res);
            else
                System.out.printf("\"%s\" => \"%s\", Correct!\n", in, expected);
        } catch (Exception e) {
            System.err.printf("Expected \"%s\", got the error \"%s\"\n", expected, e.getMessage());
        }

    }
}
