package com.fitzwilliam.lecture14.tasks;

/**
 * Given a string, return a version without the first and last char, so "Hello" yields "ell". The string length will be at least 2.
 * <p>
 * withoutEnd("Hello") → "ell"
 * withoutEnd("java") → "av"
 * withoutEnd("coding") → "odin"
 *
 * @author alisovenko
 *         12/1/16.
 */
public class SubstringPracticeWithoutEnd {
    public static String withoutEnd(String str) {
        // check for null and for string length
        // if the string length is less or equal to 2 - return ""
        // otherwise use substring() to return substring of proper size
        return null;
    }

    public static void main(String[] args) {
        checkStrings("Hello", "ell");
        checkStrings("abcd", "bc");
        checkStrings("coding", "odin");
        checkStrings("abc", "b");
        checkStrings("ab", "");
        checkStrings("a", "");
        checkStrings("", "");
        checkStrings(null, "");
    }

    private static void checkStrings(String in, String expected) {
        try {
            String res = withoutEnd(in);
            if (!res.equals(expected))
                System.err.printf("Expected \"%s\", got \"%s\"\n", expected, res);
            else
                System.out.printf("\"%s\" => \"%s\", Correct!\n", in, expected);
        } catch (Exception e) {
            System.err.printf("Expected \"%s\", got the error \"%s\"\n", expected, e.getMessage());
        }

    }
}
