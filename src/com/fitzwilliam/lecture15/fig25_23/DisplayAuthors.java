package com.fitzwilliam.lecture15.fig25_23;// Fig. 25.23: DisplayAuthors.java
// Displaying the contents of the authors table.

import java.sql.*;

public class DisplayAuthors {
    // Database URL
    static final String DATABASE_URL = "jdbc:mysql://localhost/books";

    // launch the application
    public static void main(String args[]) {
        // connect to database books and query database
        try (// establish connection to database
             Connection connection = DriverManager.getConnection(DATABASE_URL, "jhtp7", "jhtp7");
             // create Statement for querying database
             Statement statement = connection.createStatement();
             // query database
             ResultSet resultSet = statement.executeQuery("select lastname from authors")) {

            showResults(resultSet);

        }  // end try
        catch (SQLException sqlException) {
            sqlException.printStackTrace();
        } // end catch
        // end catch
    } // end main

    private static void showResults(ResultSet resultSet) throws SQLException {
        // process query results
        ResultSetMetaData metaData = resultSet.getMetaData();
        int numberOfColumns = metaData.getColumnCount();
        System.out.println("Authors Table of Books Database:\n");

        for (int i = 1; i <= numberOfColumns; i++)
            System.out.printf("%-35s\t", metaData.getColumnName(i));
        System.out.println();

        while (resultSet.next()) {
            for (int i = 1; i <= numberOfColumns; i++)
                System.out.printf("%-35s\t", resultSet.getObject(i));
            System.out.println();
        } // end while
    }
} // end class DisplayAuthors



