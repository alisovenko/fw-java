package com.fitzwilliam.lecture9.tasks._3;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author alisovenko
 *         11/15/16.
 */
public class TemperatureFrame extends JFrame {
    private JTextField textField;

    private JLabel labelFieldCelsius;
    private JLabel labelFieldKelvin;

    public TemperatureFrame() {
        super("Temperature converter");
        setLayout(new FlowLayout()); // set frame layout

        textField = new JTextField("Enter Fahrenheit", 20);
        add(textField);

        labelFieldCelsius = new JLabel("Celsius");
        add(labelFieldCelsius);

        labelFieldKelvin = new JLabel("Kelvin");
        add(labelFieldKelvin);

        textField.addActionListener(new TemperatureListener());
    }

    private class TemperatureListener implements ActionListener {
        @Override
        public void actionPerformed(ActionEvent e) {
            String text = e.getActionCommand();

            // Convert string value to double - check temperature for possible range
            Double temperature = convertTemperatureToDouble(text);
            if (temperature == null)
                return;

            // Convert fahrenheit -> celsius and celsius -> kelvin
            double celsius = convertFromFahrenheitToCelsius(temperature);
            double kelvin = convertFromCelsiusToKelvin(celsius);

            // Update matching text label fields (rounding the double to 1 digit after floating point)
            labelFieldCelsius.setText(String.format("%.1f", celsius));
            labelFieldKelvin.setText(String.format("%.1f", kelvin));
        }
    }

    /**
     * Returns the double value for temperature value as String.
     * Returns null in case any errors occurred.
     */
    private Double convertTemperatureToDouble(String text) {
        double temperature;
        try {
            temperature = Double.parseDouble(text);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Please enter digits");
            return null;
        }

        if (temperature > 100000 || temperature < -10000) {
            JOptionPane.showMessageDialog(this, "Please specify temperature in the ranges -10000 to 100000");
            return null;
        }
        return temperature;
    }

    private double convertFromCelsiusToKelvin(double result) {
        return result + 273.15;
    }

    private double convertFromFahrenheitToCelsius(double temperature) {
        return 5.0 / 9.0 * (temperature - 32);
    }
}
