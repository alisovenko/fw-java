package com.fitzwilliam.lecture9.tasks._2;

/**
 * @author alisovenko
 *         11/17/16.
 */
public class TemperatureTest {
    public static void main(String[] args) {
        new TemperatureFrame().convertFromFahrenheitToCelsius("40");
        new TemperatureFrame().convertFromFahrenheitToCelsius("40.9");
        new TemperatureFrame().convertFromFahrenheitToCelsius("0.0");
        new TemperatureFrame().convertFromFahrenheitToCelsius("-23");
        new TemperatureFrame().convertFromFahrenheitToCelsius("-23.6");
        new TemperatureFrame().convertFromFahrenheitToCelsius("32");
        new TemperatureFrame().convertFromFahrenheitToCelsius(String.valueOf(Integer.MAX_VALUE));
        new TemperatureFrame().convertFromFahrenheitToCelsius("dsfsdfds");
        new TemperatureFrame().convertFromFahrenheitToCelsius("#$%Q%#%");
    }
}
