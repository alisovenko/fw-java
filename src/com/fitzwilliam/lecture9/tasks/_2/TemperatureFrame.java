package com.fitzwilliam.lecture9.tasks._2;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author alisovenko
 *         11/15/16.
 */
public class TemperatureFrame extends JFrame {
    private JTextField textField;

    private JLabel labelField;

    public TemperatureFrame() {
        super("Temperature converter");
        setLayout(new FlowLayout()); // set frame layout

        textField = new JTextField("Enter Fahrenheit", 20);
        add(textField);

        labelField = new JLabel("Celsius");
        add(labelField);

        textField.addActionListener(new MyListener());
    }

    private class MyListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            String text = e.getActionCommand();

            double result = convertFromFahrenheitToCelsius(text);
            labelField.setText(String.format("%.1f", result));
        }
    }

    double convertFromFahrenheitToCelsius(String text) {
        double temperature = 0;
        try {
            temperature = Double.parseDouble(text);
        } catch (NumberFormatException e) {
            System.out.println("Please enter digits");
        }

        if (temperature > 100000 || temperature < -10000) {
            throw new IllegalArgumentException("Please specify temperature in the ranges -10000 to 100000");
        }

        return 5.0 / 9.0 * (temperature - 32);
    }
}
