package com.fitzwilliam.lecture9.tasks._6;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Random;

/**
 * @author alisovenko
 *         11/27/16.
 */
public class GameFrame extends JFrame {
    private JTextField guessTextField;
    private JButton startNewGameButton;
    private JLabel guessResultLabel;
    private int currNumber;
    private Integer prevNumber;
    private Color defaultBackground;

    public GameFrame() throws HeadlessException {
        super("Guess number");

        setLayout(new BorderLayout());

        // Let's save the default background to variable (used when we reset the random variable)
        defaultBackground = getContentPane().getBackground();

        // 1. "Start new game" button. Is placed in the north
        startNewGameButton = new JButton("Start new game");
        startNewGameButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                startNewGame();
            }
        });

        add(startNewGameButton, BorderLayout.NORTH);

        // 2. Label, containing the results of guessing ("Too High"/"Too Low"). Placed in the middle of the screen
        guessResultLabel = new JLabel();
        guessResultLabel.setHorizontalAlignment(SwingConstants.CENTER);
        guessResultLabel.setFont(new Font("Monospaced", Font.BOLD, 20));
        add(guessResultLabel, BorderLayout.CENTER);

        // 3. Text field for entering the guessed number (with label)
        JLabel label = new JLabel("Enter the number:");
        guessTextField = new JTextField(10);
        guessTextField.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                guessNumber();
            }
        });
        // Initially we disable the text field until the game is started
        guessTextField.setEnabled(false);
        label.setLabelFor(guessTextField);

        // Using panel to group label with text field
        JPanel guessPanel = new JPanel();
        guessPanel.add(label);
        guessPanel.add(guessTextField);
        add(guessPanel, BorderLayout.SOUTH);
    }

    private void guessNumber() {
        // Get the text value from text field and parse to int
        int newNumber;
        try {
            newNumber = Integer.parseInt(guessTextField.getText());
        } catch (NumberFormatException e) {
            // Always check for incorrect input!
            JOptionPane.showMessageDialog(this, "Enter digits!");
            return;
        }
        if (newNumber == currNumber) {
            // We guessed the number! Need to start the new game
            startNewGameButton.setEnabled(true);
            guessTextField.setText(null);
            guessTextField.setEnabled(false);
            guessResultLabel.setText("Yes! It was " + currNumber);
            this.getContentPane().setBackground(defaultBackground);
            prevNumber = null;
            return;
        }
        if (newNumber < currNumber)
            guessResultLabel.setText("Too Low");
        else
            guessResultLabel.setText("Too High");

        // We change the background only if we have previous number to compare
        // During the first num we have no previous number
        if (prevNumber != null) {
            // We got further from target number.
            if (Math.abs(newNumber - currNumber) > Math.abs(prevNumber - currNumber))
                this.getContentPane().setBackground(Color.BLUE);
            else
                this.getContentPane().setBackground(Color.RED);
        }

        // Updating the "previous" number to current guessed for future
        prevNumber = newNumber;
    }

    // Generating the next number, enabling the text field and disabling the "start" button
    private void startNewGame() {
        currNumber = new Random().nextInt(1 + 1000);
        guessTextField.setEnabled(true);
        startNewGameButton.setEnabled(false);
    }
}
