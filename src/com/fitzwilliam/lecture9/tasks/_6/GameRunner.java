package com.fitzwilliam.lecture9.tasks._6;

import javax.swing.*;

/**
 * @author alisovenko
 *         11/27/16.
 */
public class GameRunner {
    public static void main(String[] args) {
        GameFrame frame = new GameFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 200); // set frame size
        frame.setVisible(true); // display frame
    }
}
