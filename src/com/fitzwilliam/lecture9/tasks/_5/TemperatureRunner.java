package com.fitzwilliam.lecture9.tasks._5;

import javax.swing.*;

/**
 * @author alisovenko
 *         11/15/16.
 */
public class TemperatureRunner {
    public static void main(String[] args) {
        TemperatureFrame frame = new TemperatureFrame();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(600, 100); // set frame size
        frame.setVisible(true); // display frame
    }
}
