package com.fitzwilliam.lecture9.tasks._5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * @author alisovenko
 *         11/15/16.
 */
public class TemperatureFrame extends JFrame {
    private JTextField temperatureTextField;
    private JButton celsiusButton;
    private JButton kelvinButton;

    private JLabel labelField;

    public TemperatureFrame() {
        super("Temperature converter");
        setLayout(new GridLayout(1, 3)); // set frame layout

        // Adding text field for entering temperature and label for it
        temperatureTextField = new JTextField(10);
        JLabel fahrenheitLabel = new JLabel("Enter Fahrenheit");
        fahrenheitLabel.setLabelFor(temperatureTextField);

        JPanel inputPanel = new JPanel();
        inputPanel.add(fahrenheitLabel);
        inputPanel.add(temperatureTextField);
        add(inputPanel);

        // Adding two buttons for converting to celsius and kelvin
        celsiusButton = new JButton("Convert to Celsius");
        kelvinButton = new JButton("Convert to Kelvin");

        ButtonsListener temperatureConverter = new ButtonsListener();
        celsiusButton.addActionListener(temperatureConverter);
        kelvinButton.addActionListener(temperatureConverter);

        JPanel buttonPanel = new JPanel();
        buttonPanel.add(celsiusButton);
        buttonPanel.add(kelvinButton);
        add(buttonPanel);

        // Adding the label for output conversion
        labelField = new JLabel("<Temperature conversion results will be here>");
        add(labelField);
    }

    private class ButtonsListener implements ActionListener {

        @Override
        public void actionPerformed(ActionEvent e) {
            Double temperature = convertTemperatureToDouble(temperatureTextField.getText());
            if (temperature == null)
                return;

            // Example of auto unboxing: double primitive is fetched from Double object automatically
            double converted = convertFromFahrenheitToCelsius(temperature);
            ;

            // If kelvin button was clicked - just overwrite the converted value. Otherwise it contains celsius already.
            if (e.getSource() == kelvinButton) {
                converted = convertFromCelsiusToKelvin(temperature);
            }

            labelField.setText(String.format("%.1f", converted));
        }
    }

    /**
     * Returns the double value for temperature value as String.
     * Returns null in case any errors occurred.
     */
    private Double convertTemperatureToDouble(String text) {
        double temperature;
        try {
            temperature = Double.parseDouble(text);
        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(this, "Please enter digits");
            return null;
        }

        if (temperature > 100000 || temperature < -10000) {
            JOptionPane.showMessageDialog(this, "Please specify temperature in the ranges -10000 to 100000");
            return null;
        }
        return temperature;
    }

    private double convertFromCelsiusToKelvin(double result) {
        return result + 273.15;
    }

    private double convertFromFahrenheitToCelsius(double temperature) {
        return 5.0 / 9.0 * (temperature - 32);
    }
}
