package com.fitzwilliam.lecture9.fig11_19_20;// Fig. 11.20: RadioButtonTest.java
// Testing RadioButtonFrame.

import javax.swing.*;

public class RadioButtonTest {
    public static void main(String args[]) {
        RadioButtonFrame radioButtonFrame = new RadioButtonFrame();
        radioButtonFrame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        radioButtonFrame.setSize(300, 100); // set frame size
        radioButtonFrame.setVisible(true); // display frame
    } // end main
} // end class RadioButtonTest 
