package com.fitzwilliam.assignments._1;

/**
 * @author alisovenko
 * 10/31/16.
 */

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class DrawPanel extends JPanel {
    private Random rand = new Random();
    private MyLine lines[]; // array of lines
    private MyRect rects[]; // array of rects
    private MyOval ovals[]; // array of ovals

    // constructor, creates a panel with random shapes
    public DrawPanel() {
        setBackground(Color.WHITE);

        lines = new MyLine[1 + rand.nextInt(5)];
        rects = new MyRect[1 + rand.nextInt(5)];
        ovals = new MyOval[1 + rand.nextInt(5)];

        // create lines
        for (int count = 0; count < lines.length; count++) {
            // generate random coordinates
            int[] coords = generateCoords(300);

            // add the line to the list of lines to be displayed
            lines[count] = new MyLine(coords[0], coords[1], coords[2], coords[3], generateColor());
        }

        // create rectangles
        for (int count = 0; count < rects.length; count++) {
            // generate random coordinates
            int[] coords = generateCoords(300);

            // add the rectangle to the list of rectangles to be displayed
            rects[count] = new MyRect(coords[0], coords[1], coords[2], coords[3], generateColor(), rand.nextBoolean());
        }
        // create ovals
        for (int count = 0; count < ovals.length; count++) {
            // generate random coordinates
            int[] coords = generateCoords(300);

            // add the rectangle to the list of rectangles to be displayed
            ovals[count] = new MyOval(coords[0], coords[1], coords[2], coords[3], generateColor(), rand.nextBoolean());
        }
    }

    private Color generateColor() {
        return new Color(rand.nextInt(256),
                rand.nextInt(256), rand.nextInt(256));
    }

    private int[] generateCoords(int upperLimit) {
        return new int[]{rand.nextInt(upperLimit), rand.nextInt(upperLimit), rand.nextInt(upperLimit),
                rand.nextInt(upperLimit)};
    }

    // for each shape array, draw the individual shapes
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        // draw the lines
        for (MyLine line : lines)
            line.draw(g);

        // draw the rectangles
        for (MyRect rect : rects)
            rect.draw(g);

        // draw the ovals
        for (MyOval oval : ovals)
            oval.draw(g);
    }
}