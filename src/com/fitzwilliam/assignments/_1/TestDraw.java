package com.fitzwilliam.assignments._1;

import javax.swing.*;

/**
 * @author alisovenko
 *         10/31/16.
 */
public class TestDraw {
    public static void main(String args[]) {
        DrawPanel panel = new DrawPanel();
        JFrame application = new JFrame();

        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        application.add(panel);
        application.setSize(300, 300);
        application.setVisible(true);
    }
}
