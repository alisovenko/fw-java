//by Fitzwilliam Institute
// LabelDemo.java
// Demonstrates the use of labels.
package com.fitzwilliam.assignments._2;

import javax.swing.*;
import java.awt.*;

// Base for assignment 2
// Fig 9.19: LabelDemo.java
public class LabelDemo {

    public static void main(String[] args) {

        // Create a label with plain text
        JLabel northLabel = new JLabel("North");

        // create an icon from an image so we can put it on a JLabel
        ImageIcon labelIcon = new ImageIcon("sun.gif");

        // create a label with an Icon instead of text
        JLabel centerLabel = new JLabel(labelIcon);

        // create another label with an Icon
        JLabel southLabel = new JLabel(labelIcon);

        // set the label to display text (as well as an icon)
        southLabel.setText("South");

        // create a frame to hold the labels
        JFrame application = new JFrame();

        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);

        // add the labels to the frame; the second argument specifies
        // where on the frame to add the label 
        application.add(northLabel, BorderLayout.NORTH);
        application.add(centerLabel, BorderLayout.CENTER);
        application.add(southLabel, BorderLayout.SOUTH);

        application.setSize(500, 500); // set the size of the frame
        application.setVisible(true); // show the frame
    } // end main
} // end class LabelDemo

