package com.fitzwilliam.assignments._2;

import javax.swing.*;
import java.awt.*;

/**
 * @author alisovenko
 *         10/31/16.
 */
public class TestDraw {
    public static void main(String args[]) {
        DrawPanel panel = new DrawPanel();
        JFrame application = new JFrame();

        String numberOfShapes = panel.numberOfShapes();
        JLabel label = new JLabel(numberOfShapes);
        JLabel label2 = new JLabel(numberOfShapes);

        application.add(label, BorderLayout.SOUTH);
        application.add(label2, BorderLayout.NORTH);

        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        application.add(panel);
        application.setSize(300, 300);
        application.setVisible(true);
    }
}
