package com.fitzwilliam.assignments._2;

import java.awt.*;

/**
 * @author alisovenko
 *         11/7/16.
 */
public class MyOval {
    private int x1;
    private int y1;
    private int x2;
    private int y2;
    private Color myColor;
    private boolean filled;

    public MyOval(int x1, int y1, int x2, int y2, Color myColor, boolean filled) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.myColor = myColor;
        this.filled = filled;
    }

    public int getUpperLeftX() {
        return Math.min(x1, x2);
    }

    public int getUpperLeftY() {
        return Math.min(y1, y2);
    }

    public int getWidth() {
        return Math.abs(x1 - x2);
    }

    public int getHeight() {
        return Math.abs(y1 - y2);
    }

    void draw(Graphics g) {
        g.setColor(myColor);
        g.drawOval(getUpperLeftX(), getUpperLeftY(), getWidth(), getHeight());
        if (filled)
            g.fillOval(getUpperLeftX(), getUpperLeftY(), getWidth(), getHeight());
    }
}
