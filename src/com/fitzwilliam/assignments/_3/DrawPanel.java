package com.fitzwilliam.assignments._3;

/**
 * @author alisovenko
 * 10/31/16.
 */

import javax.swing.*;
import java.awt.*;
import java.util.Random;

public class DrawPanel extends JPanel {
    private Random rand = new Random();
    private MyShape figures[];
    private int linesCnt, ovalsCnt, rectCnt;

    // constructor, creates a panel with random shapes
    public DrawPanel(int numberOfShapes) {
        setBackground(Color.WHITE);

        figures = new MyShape[numberOfShapes];

        // create lines
        for (int count = 0; count < figures.length; count++) {
            // generate random coordinates
            int[] coords = generateCoords(300);

            // add the line to the list of lines to be displayed
            int nextType = rand.nextInt(3);
            if (nextType == 0) {
                figures[count] = new MyLine(coords[0], coords[1], coords[2], coords[3], generateColor());
                linesCnt++;
            } else if (nextType == 1) {
                figures[count] = new MyRect(coords[0], coords[1], coords[2], coords[3], generateColor(), rand.nextBoolean());
                rectCnt++;
            } else {
                figures[count] = new MyOval(coords[0], coords[1], coords[2], coords[3], generateColor(), rand.nextBoolean());
                ovalsCnt++;
            }
        }
    }

    private Color generateColor() {
        return new Color(rand.nextInt(256),
                rand.nextInt(256), rand.nextInt(256));
    }

    private int[] generateCoords(int upperLimit) {
        return new int[]{rand.nextInt(upperLimit), rand.nextInt(upperLimit), rand.nextInt(upperLimit),
                rand.nextInt(upperLimit)};
    }

    public String numberOfShapes() {
        return String.format("There are %d oval(s), %d rectangle(s) and %d line(s)", ovalsCnt, rectCnt, linesCnt);
    }

    // for each shape array, draw the individual shapes
    public void paintComponent(Graphics g) {
        super.paintComponent(g);

        // draw the lines
        for (MyShape figure : figures)
            figure.draw(g);
    }
}