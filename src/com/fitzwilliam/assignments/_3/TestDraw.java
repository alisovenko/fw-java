package com.fitzwilliam.assignments._3;

import javax.swing.*;
import java.awt.*;

/**
 * @author alisovenko
 *         10/31/16.
 */
public class TestDraw {
    public static void main(String args[]) {
        String errorMsg = null;
        int figuresNumber = getFiguresNumberFromUser(errorMsg);

        JFrame application = new JFrame();
        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        application.setSize(300, 300);

        DrawPanel panel = new DrawPanel(figuresNumber);
        application.add(panel);

        String numberOfShapes = panel.numberOfShapes();
        JLabel label = new JLabel(numberOfShapes);
        JLabel label2 = new JLabel(numberOfShapes);

        application.add(label, BorderLayout.SOUTH);
        application.add(label2, BorderLayout.NORTH);

        application.setVisible(true);

    }

    private static int getFiguresNumberFromUser(String errorMsg) {
        int figuresNumber = 10;

        do {
            String number = JOptionPane.showInputDialog(String.format("Enter the number of figures to generate. \n%s", errorMsg == null ? "" : errorMsg));

            try {
                figuresNumber = Integer.parseInt(number);
                if (figuresNumber <= 0 || figuresNumber > 50)
                    errorMsg = "Please enter the number greater than 0!";
                else
                    errorMsg = null;
            } catch (NumberFormatException e) {
                errorMsg = "The data you've entered contains non-numeric symbols!";
            }
            // Cancel
            if (number == null)
                System.exit(0);
        } while (errorMsg != null);
        return figuresNumber;
    }
}
