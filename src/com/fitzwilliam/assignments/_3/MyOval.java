package com.fitzwilliam.assignments._3;

import java.awt.*;

/**
 * @author alisovenko
 *         11/7/16.
 */
public class MyOval extends MyShape {
    private boolean filled;

    protected MyOval(int x1, int y1, int x2, int y2, Color myColor, boolean filled) {
        super(x1, y1, x2, y2, myColor);
        this.filled = filled;
    }

    public int getUpperLeftX() {
        return Math.min(getX1(), getX2());
    }

    public int getUpperLeftY() {
        return Math.min(getY1(), getY2());
    }

    public int getWidth() {
        return Math.abs(getX1() - getX2());
    }

    public int getHeight() {
        return Math.abs(getY1() - getY2());
    }

    @Override
    void draw(Graphics g) {
        g.setColor(getMyColor());
        g.drawOval(getUpperLeftX(), getUpperLeftY(), getWidth(), getHeight());
        if (filled)
            g.fillOval(getUpperLeftX(), getUpperLeftY(), getWidth(), getHeight());
    }
}
