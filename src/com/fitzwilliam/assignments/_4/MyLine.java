package com.fitzwilliam.assignments._4;

import java.awt.*;

/**
 * @author alisovenko
 *         10/31/16.
 */
public class MyLine extends MyShape {
    public MyLine(int x1, int y1, int x2, int y2, Color color) {
        super(x1, y1, x2, y2, color);
    }

    // Actually draws the line
    @Override
    public void draw(Graphics g) {
        g.setColor(getMyColor());
        g.drawLine(getX1(), getY1(), getX2(), getY2());
    }
}