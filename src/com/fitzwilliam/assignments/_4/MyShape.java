package com.fitzwilliam.assignments._4;

import java.awt.*;

/**
 * @author alisovenko
 *         11/7/16.
 */
public abstract class MyShape {
    private int x1;
    private int y1;
    private int x2;
    private int y2;
    private Color myColor;

    protected MyShape(int x1, int y1, int x2, int y2, Color myColor) {
        this.x1 = x1;
        this.y1 = y1;
        this.x2 = x2;
        this.y2 = y2;
        this.myColor = myColor;
    }

    public int getX1() {
        return x1;
    }

    public int getY1() {
        return y1;
    }

    public int getX2() {
        return x2;
    }

    public int getY2() {
        return y2;
    }

    public Color getMyColor() {
        return myColor;
    }

    abstract void draw(Graphics g);

    @Override
    public String toString() {
        return "[" +
                "x1=" + x1 +
                ", y1=" + y1 +
                ", x2=" + x2 +
                ", y2=" + y2 +
                ", myColor=" + myColor +
                ']';
    }
}
