package com.fitzwilliam.assignments._5;

import java.awt.*;

/**
 * @author alisovenko
 *         11/7/16.
 */
public class MyRect extends MyBoundedShape {
    public MyRect(int x1, int y1, int x2, int y2, Color myColor, boolean filled) {
        super(x1, y1, x2, y2, myColor, filled);
    }

    public MyRect() {
        super(0, 0, 0, 0, Color.BLACK, false);
    }

    @Override
    void draw(Graphics g) {
        g.setColor(getMyColor());
        g.drawRect(getUpperLeftX(), getUpperLeftY(), getWidth(), getHeight());
        if (filled)
            g.fillRect(getUpperLeftX(), getUpperLeftY(), getWidth(), getHeight());
    }
}
