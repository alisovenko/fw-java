package com.fitzwilliam.assignments._5;

/**
 * @author alisovenko
 *         11/27/16.
 */
public enum ShapeType {
    OVAL("Oval", MyOval.class), RECT("Rectangle", MyRect.class), LINE("Line", MyLine.class);

    private final String label;
    private final Class<? extends MyShape> shapeClass;

    ShapeType(String label, Class<? extends MyShape> shapeClass) {
        this.label = label;
        this.shapeClass = shapeClass;
    }

    public String getLabel() {
        return label;
    }

    @Override
    public String toString() {
        return label;
    }

    public MyShape createShape() {
        try {
            return shapeClass.newInstance();
        } catch (InstantiationException | IllegalAccessException e) {
            throw new IllegalStateException();
        }
    }
}
