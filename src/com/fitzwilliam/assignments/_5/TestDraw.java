package com.fitzwilliam.assignments._5;

import javax.swing.*;

/**
 * @author alisovenko
 *         11/27/16.
 */
public class TestDraw {
    public static void main(String[] args) {
        DrawFrame application = new DrawFrame();
        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        application.setSize(600, 300);
        application.setVisible(true);
    }
}
