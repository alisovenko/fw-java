package com.fitzwilliam.assignments._5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * @author alisovenko
 *         11/27/16.
 */
public class DrawFrame extends JFrame {

    public static final String[] COLOR_NAMES = {"Black", "Red", "Yellow", "Green", "Blue"};
    public static final Color[] COLORS= {Color.BLACK, Color.RED, Color.YELLOW, Color.GREEN, Color.BLUE};

    public DrawFrame() {
        super("Drawing panel");
        setLayout(new BorderLayout());

        JLabel label = new JLabel();
        DrawPanel drawPanel = new DrawPanel(label);
        JPanel menuPanel = createMenuPanel(drawPanel);

        add(menuPanel, BorderLayout.NORTH);
        add(drawPanel, BorderLayout.CENTER);
        add(label, BorderLayout.SOUTH);
    }

    private JPanel createMenuPanel(DrawPanel drawPanel) {
        JPanel menuPanel = new JPanel();
        menuPanel.setLayout(new FlowLayout());
        menuPanel.setAlignmentX(SwingConstants.CENTER);

        menuPanel.add(createUndoButton(drawPanel));
        menuPanel.add(createClearButton(drawPanel));
        menuPanel.add(createColorDropDown(drawPanel));
        menuPanel.add(createShapeDropDown(drawPanel));
        menuPanel.add(createFilledCheckbox(drawPanel));

        return menuPanel;
    }

    private JCheckBox createFilledCheckbox(DrawPanel drawPanel) {
        JCheckBox checkBox = new JCheckBox("Filled");
        checkBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                drawPanel.setFilledShape(checkBox.isSelected());
            }
        });
        return checkBox;
    }

    private JComboBox<ShapeType> createShapeDropDown(DrawPanel drawPanel) {
        JComboBox<ShapeType> jComboBox = new JComboBox<>(ShapeType.values());
        jComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED)
                    drawPanel.setShapeType((ShapeType) e.getItem());
            }
        });
        return jComboBox;
    }

    private JComboBox<String> createColorDropDown(DrawPanel drawPanel) {
        JComboBox<String> jComboBox = new JComboBox<>(COLOR_NAMES);

        jComboBox.addItemListener(new ItemListener() {
            @Override
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED)
                    drawPanel.setCurrentColor(COLORS[jComboBox.getSelectedIndex()]);
            }
        });
        return jComboBox;
    }

    private JButton createClearButton(DrawPanel drawPanel) {
        JButton undoButton = new JButton("Clear");
        undoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                drawPanel.clearDrawing();
            }
        });
        return undoButton;
    }

    private JButton createUndoButton(DrawPanel drawPanel) {
        JButton undoButton = new JButton("Undo");
        undoButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                drawPanel.clearLastShape();
            }
        });
        return undoButton;
    }
}
