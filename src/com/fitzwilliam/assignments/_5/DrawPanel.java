package com.fitzwilliam.assignments._5;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * @author alisovenko
 *         11/27/16.
 */
public class DrawPanel extends JPanel {
    private MyShape[] shapes;
    private int shapeCount;
    private ShapeType shapeType;
    private MyShape currentShape;
    private Color currentColor;
    private boolean filledShape;
    private JLabel statusLabel;

    public DrawPanel(JLabel statusLabel) {
        setBackground(Color.WHITE);
        this.statusLabel = statusLabel;
        shapes = new MyShape[100];
        shapeCount = 0;
        shapeType = ShapeType.OVAL;
        currentColor = Color.BLACK;

        MouseListener l = new MouseListener();
        addMouseListener(l);
        addMouseMotionListener(l);
    }

    public void setShapeType(ShapeType shapeType) {
        this.shapeType = shapeType;
    }

    public void setCurrentColor(Color currentColor) {
        this.currentColor = currentColor;
    }

    public void setFilledShape(boolean filledShape) {
        this.filledShape = filledShape;
    }

    public void clearLastShape() {
        if (shapeCount > 0) {
            shapeCount--;
            repaint();
        }
    }

    public void clearDrawing() {
        shapeCount = 0;
        repaint();
    }

    @Override
    protected void paintComponent(Graphics g) {
        super.paintComponent(g);

        for (int i = 0; i < shapeCount; i++) {
            shapes[i].draw(g);
        }
        if (currentShape != null)
            currentShape.draw(g);
    }

    private class MouseListener extends MouseAdapter {
        @Override
        public void mousePressed(MouseEvent e) {
            currentShape = shapeType.createShape();
            currentShape.setX1(e.getX());
            currentShape.setY1(e.getY());
            currentShape.setMyColor(currentColor);
            if (currentShape instanceof MyBoundedShape)
                ((MyBoundedShape)currentShape).setFilled(filledShape);

        }

        @Override
        public void mouseReleased(MouseEvent e) {
            currentShape.setX2(e.getX());
            currentShape.setY2(e.getY());

            shapes[shapeCount++] = currentShape;
            currentShape = null;
            repaint();
        }

        @Override
        public void mouseMoved(MouseEvent e) {
            statusLabel.setText(String.format("(%d, %d)", e.getX(), e.getY()));
        }

        @Override
        public void mouseDragged(MouseEvent e) {
            currentShape.setX2(e.getX());
            currentShape.setY2(e.getY());
            repaint();
            statusLabel.setText(String.format("(%d, %d)", e.getX(), e.getY()));
        }
    }
}
