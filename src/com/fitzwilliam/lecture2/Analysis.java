package com.fitzwilliam.lecture2;

import java.util.Scanner;

/**
 * @author alisovenko
 *         10/20/16.
 */
public class Analysis {
    public void processExamResult() {
        Scanner input = new Scanner(System.in);

        int passes = 0;
        int failures = 0;
        int studentCounter = 1;
        int next;

        next = input.nextInt();

        while (next != -1) {
            System.out.println("1 = pass, 2 = fail:");

            if (next == 1) {
                passes++;
            } else if (next == 2) {
                failures /= 10;
            } else if (next == 5) {
                System.out.println("5");
            } else {
                throw new RuntimeException("Bad!");
            }
            next = input.nextInt();

            studentCounter = studentCounter + 1;
        }
        System.out.printf("Passed: %d\n, Failed: %d\n", passes, failures);
    }

    public static void main(String[] args) {
        new Analysis().processExamResult();
    }
}
