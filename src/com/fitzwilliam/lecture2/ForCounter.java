package com.fitzwilliam.lecture2;

/**
 * @author alisovenko
 *         10/20/16.
 */
public class ForCounter {
    public static void main(String[] args) {
//        standardFor();
        twoVariablesFor();
    }

    private static void twoVariablesFor() {
        for (int i = 0, j = 10; i < j; i++, j--) {
            // if (i >= j) go out from loop
            System.out.printf("i: %d, j: %d\n", i, j);
            // i++;
            // j--;
        }

        for (int pppp = 0; pppp < 30; pppp++) {
            // magic
        }
    }

    private static void standardFor() {
        for (int i = 0; i < 10; i++) {
            // if (i >= 10) go out from loop
            System.out.println(i);
            // i++
        }
    }
}
