package com.fitzwilliam.lecture2;

/**
 * @author alisovenko
 *         10/20/16.
 */
public class Increment {
    public static void main(String[] args) {
        int c = 5;

        System.out.println(c);
        System.out.println(c++); // System.out.println(c = 5); c = c + 1;
        System.out.println(c);

        System.out.println();

        c = 5;
        System.out.println(c);
        System.out.println(++c); // System.out.println(c = c + 1)
        System.out.println(c);
    }
}
