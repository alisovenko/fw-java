package com.fitzwilliam.lecture2;

import java.util.Scanner;

/**
 * @author alisovenko
 *         10/20/16.
 */
public class Analysis2 {
    public void processExamResult() {
        Scanner input = new Scanner(System.in);

        int passes = 0;
        int failures = 0;
        int studentCounter = 1;
        int next;

        next = input.nextInt();

        while (next != -1) {
            System.out.println("1 = pass, 2 = fail:");

            passes += (next == 1) ? 1 : 0;
            failures += (next == 2) ? 1 : 0;

            studentCounter = studentCounter + 1;
        }
        System.out.printf("Passed: %d\n, Failed: %d\n", passes, failures);
    }

    public static void main(String[] args) {
        new Analysis2().processExamResult();
    }
}
