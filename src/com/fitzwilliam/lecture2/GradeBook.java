package com.fitzwilliam.lecture2;

import java.util.Scanner;

/**
 * @author alisovenko
 *         10/20/16.
 */
public class GradeBook {
    private String courseName;
    private int total;
    private int gradeCounter;
    private int aCount;
    private int bCount;
    private int cCount;
    private int dCount;
    private int fCount;

    public GradeBook(String name) {
        courseName = name;
    }

    public void setCourseName(String name) {
        courseName = name;
    }

    public String getCourseName() {
        return courseName;
    }

    public void determineClassAverage() {
        Scanner scanner = new Scanner(System.in);

        int total = 0;
        int gradeCounter = 1;
        int grade, average;

        while (gradeCounter <= 10) {
            System.out.println("Enter grade:");
            grade = scanner.nextInt();
            total = total + grade;
            gradeCounter = gradeCounter + 1;
        }

        average = total / 10;

        System.out.printf("\n Total of all 10 grades: %d\n", total);
        System.out.printf("\n Class average: %d\n", average);
    }

    public void inputGrades() {
        Scanner input = new Scanner(System.in);
        int grade;

        while (input.hasNext()) {
            grade = input.nextInt();
            total += grade;
            ++gradeCounter;

            incrementLetterGradeCounter(grade);
        }
    }

    private void incrementLetterGradeCounter(int grade) {
        String s = "";
        switch (grade / 10) {
            case 9:
            case 10:
                ++aCount;
                break;
            case 8:
                ++bCount;
                break;
            case 7:
                ++cCount;
                break;
            case 6:
                ++dCount;
                break;
            default:
                ++fCount;
                break;
        }
    }

    public static void main(String[] args) {
        GradeBook gradeBook = new GradeBook("Some good course");

//        gradeBook.determineClassAverage();
//        gradeBook.
    }
}
