package com.fitzwilliam.lecture16.tasks;

import java.sql.SQLException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

/**
 * @author alisovenko
 *         12/7/16.
 */
public class QueryBooksRunner {
    public static void main(String[] args) throws SQLException {
        QueryBooks queryBooks = null;
        try {
            queryBooks = new QueryBooks();

            check(queryBooks.queryBooksByEditionNumberAndCopyright(2, 2006), Arrays.asList("0131525239"));
            check(queryBooks.queryBooksByEditionNumberAndCopyright(3, 2004), Arrays.asList("0131450913", "0131828274"));
            check(queryBooks.queryBooksByEditionNumberAndCopyright(1, 2007), Collections.emptyList());
        } finally {
            if (queryBooks != null)
                queryBooks.closeResources();
        }
    }

    private static void check(List<String> fact, List<String> expected) {
        try {
            if (!fact.equals(expected))
                System.out.printf("Wrong! Expected: %s but got %s\n", expected, fact);
            else
                System.out.printf("Correct! (%s)\n", fact);
        } catch (Exception e) {
            System.out.printf("Wrong! Expected: %s but got exception: %s\n", expected, e.getMessage());
        }
    }
}
