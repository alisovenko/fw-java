package com.fitzwilliam.lecture16.tasks;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

/**
 * @author alisovenko
 *         12/7/16.
 */
public class QueryBooks {
    static final String DATABASE_URL = "jdbc:mysql://localhost/books";
    private Connection conn;
    private PreparedStatement sql;

    public QueryBooks() throws SQLException {
        conn = DriverManager.getConnection(DATABASE_URL, "jhtp7", "jhtp7");
        sql = conn.prepareStatement("select isbn from titles where editionNumber = ? and copyRight = ? order by isbn ASC ");
    }

    /**
     * Queries database table books to fetch all books which editionNumber is equal to {@code editionNumber} and copyright is equal to
     * {@code copyright}.
     *
     * It returns the list of books ISBNs
     */
    public List<String> queryBooksByEditionNumberAndCopyright(int editionNumber, int copyRight) throws SQLException {
        List<String> result =new ArrayList<>();

        sql.setInt(1, editionNumber);
        sql.setInt(2, copyRight);

        try (ResultSet rs = sql.executeQuery()) {
            while (rs.next())
                result.add(rs.getString(1));
        }

        return result;
    }

    public void closeResources() {
        try {
            sql.close();
        } catch (SQLException e) {
            // no-op
        }
        try {
            conn.close();
        } catch (SQLException e) {
            // no-op
        }
    }
}
