package com.fitzwilliam.lecture7.payable;
// Payable interface declaration.

public interface Payable {
    double getPaymentAmount(); // calculate payment; no implementation
}
