package com.fitzwilliam.lecture7.tasks;
// Employee abstract superclass.

import java.time.LocalDate;

public abstract class Employee {
    private String firstName;
    public String lastName;
    private String socialSecurityNumber;
    // Final means that the field cannot be updated after object is created (only through constructor)
    private final LocalDate birthday;

    // three-argument constructor
    // Note, that after we changed the constructor (added additional parameter) all subclasses stopped to be compiled as they referenced super(..) with three parameters
    public Employee(String first, String last, String ssn, LocalDate birthday) {
        firstName = first;
        lastName = last;
        socialSecurityNumber = ssn;
        // The only way to initialise this field is from constructor
        this.birthday = birthday;
    } // end three-argument Employee constructor

    // return first name
    public String getFirstName() {
        return firstName;
    }

    // return last name
    public String getLastName() {
        return lastName;
    }

    // return social security number
    public String getSocialSecurityNumber() {
        return socialSecurityNumber;
    }

    // return String representation of Employee object
    public String toString() {
        return String.format("%s %s\nsocial security number: %s\nbirthday: %s\n",
                getFirstName(), getLastName(), getSocialSecurityNumber(), birthday);
    }

    // abstract method overridden by subclasses (no implementation!)
    public abstract double weaklyEarnings();

    public LocalDate getBirthday() {
        return birthday;
    }
}

