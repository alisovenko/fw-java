package com.fitzwilliam.lecture7.tasks;

import java.time.LocalDate;

public class PayrollSystemTest {
    public static void main(String args[]) {
        // create subclass objects
        SalariedEmployee salariedEmployee =
                new SalariedEmployee("John", "Smith", "111-11-1111", 800.00, LocalDate.parse("1988-10-05"));
        HourlyEmployee hourlyEmployee =
                new HourlyEmployee("Karen", "Price", "222-22-2222", 16.75, 40, LocalDate.parse("1983-10-05"));
        CommissionEmployee commissionEmployee =
                new CommissionEmployee(
                        "Sue", "Jones", "333-33-3333", 10000, .06, LocalDate.parse("1990-10-05"));
        BasePlusCommissionEmployee basePlusCommissionEmployee =
                new BasePlusCommissionEmployee(
                        "Bob", "Lewis", "444-44-4444", 5000, .04, 300, LocalDate.parse("1992-01-08"));

        // create four-element Employee array
        Employee employees[] = new Employee[4];

        // initialize array with Employees
        employees[0] = salariedEmployee;
        employees[1] = hourlyEmployee;
        employees[2] = commissionEmployee;
        employees[3] = basePlusCommissionEmployee;

        System.out.println("==========================================");
        System.out.println("Employees processed polymorphically:\n");

        // generically process each element in array employees
        for (Employee currentEmployee : employees) {
            System.out.println(currentEmployee); // invokes toString

            // Comparing the month of birthday of current employee against current date
            // LocalDate.now() returns the current date. It's the static method which creates new instance of LocalDate object each time it's invoked
            // Under the hood internally it still calls "new LocalDate(...)"
            // Static methods for creation of objects are well-known pattern for making developers' life more convenient
            if (currentEmployee.getBirthday().getMonth() == LocalDate.now().getMonth()) {
                // If employee's birthday is in current month - increasing his earnings to 100 euros
                System.out.printf("earned $%,.2f\n\n", (currentEmployee.weaklyEarnings() * 4) + 100);
            } else {
                System.out.printf("earned $%,.2f\n\n", currentEmployee.weaklyEarnings() * 4);
            }

        } // end for
    }
}
