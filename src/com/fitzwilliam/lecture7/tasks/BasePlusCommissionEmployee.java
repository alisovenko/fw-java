package com.fitzwilliam.lecture7.tasks;
// BasePlusCommissionEmployee class extends CommissionEmployee.

import java.time.LocalDate;

public class BasePlusCommissionEmployee extends CommissionEmployee {
    private double baseSalary; // base salary per week

    // six-argument constructor
    public BasePlusCommissionEmployee(String first, String last,
                                      String ssn, double sales, double rate, double salary, LocalDate birth) {
        super(first, last, ssn, sales, rate, birth);
        setBaseSalary(salary); // validate and store base salary
    }

    // set base salary
    public void setBaseSalary(double salary) {
        baseSalary = (salary < 0.0) ? 0.0 : salary; // non-negative
    }

    // return base salary
    public double getBaseSalary() {
        return baseSalary;
    }

    // calculate weaklyEarnings; override method weaklyEarnings in CommissionEmployee
    public double weaklyEarnings() {
        return getBaseSalary() + super.weaklyEarnings();
    }

    // return String representation of BasePlusCommissionEmployee object
    public String toString() {
        return String.format("%s %s; %s: $%,.2f",
                "base-salaried", super.toString(),
                "base salary", getBaseSalary());
    }
}

