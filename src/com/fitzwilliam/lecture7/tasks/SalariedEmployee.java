package com.fitzwilliam.lecture7.tasks;
// SalariedEmployee class extends Employee.

import java.time.LocalDate;

public class SalariedEmployee extends Employee {
    private double weeklySalary;

    // five-argument constructor
    public SalariedEmployee(String first, String last, String ssn,
                            double salary, LocalDate birth) {
        super(first, last, ssn, birth); // pass to Employee constructor
        setWeeklySalary(salary); // validate and store salary
    }

    // set salary
    public void setWeeklySalary(double salary) {
        weeklySalary = salary < 0.0 ? 0.0 : salary;
    }

    // return salary
    public double getWeeklySalary() {
        return weeklySalary;
    }

    // calculate weaklyEarnings; override abstract method weaklyEarnings in Employee
    @Override
    public double weaklyEarnings() {
        return getWeeklySalary();
    }

    // return String representation of SalariedEmployee object
    public String toString() {
        return String.format("salaried employee: %s\n%s: $%,.2f",
                super.toString(), "weekly salary", getWeeklySalary());
    }
}

