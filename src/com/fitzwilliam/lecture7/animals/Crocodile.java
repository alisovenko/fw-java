package com.fitzwilliam.lecture7.animals;

/**
 * @author alisovenko
 *         11/8/16.
 */
public class Crocodile extends Animal {
    @Override
    public boolean isHotBlooded() {
        return false;
    }
}
