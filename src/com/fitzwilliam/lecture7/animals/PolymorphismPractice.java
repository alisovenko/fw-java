package com.fitzwilliam.lecture7.animals;

/**
 * @author alisovenko
 *         11/8/16.
 */
public class PolymorphismPractice {
    public void sign(Animal animal) {
        if (animal instanceof Cat) {
            Cat c = (Cat) animal;
        }
    }

    public static void main(String[] args) {
        Animal a1 = new Cat();
        Animal a2 = new Dog();
        Animal a3 = new Crocodile();
        Animal d1 = new Dog();

        // ClassCastException as d1 is Dog
//        Cat c = (Cat) d1;
//        Animal a3 = new Animal();

        System.out.println(a1.isHotBlooded());
        System.out.println(a2.isHotBlooded());
        System.out.println(a3.isHotBlooded());
//        System.out.println(a3);
    }
}
