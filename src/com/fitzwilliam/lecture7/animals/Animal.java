package com.fitzwilliam.lecture7.animals;

/**
 * @author alisovenko
 *         11/8/16.
 */
public abstract class Animal {
    private int age;

    public int getAge() {
        return age;
    }

    public abstract boolean isHotBlooded();

    @Override
    public String toString() {
        return "from Animal";
    }
}
