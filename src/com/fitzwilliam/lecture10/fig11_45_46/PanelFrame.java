package com.fitzwilliam.lecture10.fig11_45_46;// Fig. 11.43: PanelFrame.java
// Using a JPanel to help lay out components.

import javax.swing.*;
import java.awt.*;

public class PanelFrame extends JFrame {
    private JPanel buttonJPanel; // panel to hold buttons
    private JButton buttons[]; // array of buttons

    // no-argument constructor
    public PanelFrame() {
        super("Panel Demo");
        buttons = new JButton[5]; // create buttons array
        buttonJPanel = new JPanel(); // set up panel
        buttonJPanel.setLayout(new GridLayout(1, buttons.length));

        // create and add buttons
        for (int count = 0; count < buttons.length; count++) {
            buttons[count] = new JButton("Button " + (count + 1));
            buttonJPanel.add(buttons[count]); // add button to panel
        } // end for

        add(buttonJPanel, BorderLayout.SOUTH); // add panel to JFrame
    } // end PanelFrame constructor
} // end class PanelFrame

