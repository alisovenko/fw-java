package com.fitzwilliam.lecture12.others;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * Class shows how to write to binary files using OutputStream implementations.
 *
 * @author alisovenko
 *         11/23/16.
 */
public class OuputStreamPractice {
    public static void main(String[] args) throws IOException {
        BufferedOutputStream bout = null;
        try {
            // Create array of bytes (using hexadecimal literals to create single bytes)
            byte[] bytes = new byte[]{0x23, 0x63, 0x01, 0x44};

            // We can create the FileOutputStream manually, or can ask Files class to do it
            OutputStream ous = Files.newOutputStream(Paths.get("some.file"), StandardOpenOption.CREATE);

            // We "wrap" the FileOutputStream to BufferedOutputStream for better performance
            bout = new BufferedOutputStream(ous);

            // Write bytes array to output
            bout.write(bytes);

            // For BufferedOutputStream we must always flush the in-memory buffer
            bout.flush();
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            // Always close streams after closing! Otherwise you'll run out of OS file descriptors eventually!
            if (bout != null)
                bout.close();
        }
    }
}
