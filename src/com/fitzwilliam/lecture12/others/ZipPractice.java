package com.fitzwilliam.lecture12.others;

import java.io.BufferedOutputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;

/**
 * @author alisovenko
 *         11/24/16.
 */
public class ZipPractice {
    public static void main(String[] args) {
        try (FileOutputStream fout = new FileOutputStream("sun.zip");
             BufferedOutputStream bout = new BufferedOutputStream(fout);
             ZipOutputStream zout = new ZipOutputStream(bout)) {

            // Creating entry (file) which will be in zip file
            ZipEntry ze = new ZipEntry("sun.gif");

            // Add the entry to zip output stream
            zout.putNextEntry(ze);

            // Copy contents of sun.gif to zipoutputstream. It will add it to last entry in zip archive
            Files.copy(Paths.get("sun.gif"), zout);
        } catch (IOException e) {
            System.err.println("Failed to write to 'sun-copy.zip'");
            e.printStackTrace();
        }
    }
}
