package com.fitzwilliam.lecture12.others;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;

/**
 * @author alisovenko
 *         11/24/16.
 */
public class WriteToFilePractice {
    public static void main(String[] args) {
        try {
            Files.write(Paths.get("test.txt"), Arrays.asList("Hi, how are you?", "Was nice to see you, bye!"));
        } catch (IOException e) {
            System.out.println("Failed to write text to file");
            e.printStackTrace();
        }
    }
}
