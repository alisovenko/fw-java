package com.fitzwilliam.lecture12.others;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;

/**
 * @author alisovenko
 *         11/23/16.
 */
public class InputStreamPractice {
    public static void main(String[] args) {
        try {
            // The simplest way to read binary file to bytes - use java library for this
            // The problem with manual reading - we need to create the byte array of suitable size, but we usually don't know the size of data we read.
            // So we would have to create some byte[] array and then create the bigger one and copy previous data there in case file has larger size
            byte[] bytes = Files.readAllBytes(Paths.get("sun.gif"));

            System.out.println(bytes.length);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
