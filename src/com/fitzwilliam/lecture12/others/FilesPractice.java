package com.fitzwilliam.lecture12.others;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * @author alisovenko
 *         11/23/16.
 */
public class FilesPractice {
    public static void main(String[] args) {
        // Path class is new class from java 7 and allows to work flexibly with any paths (subpath, merge, resolve, get i member)
        // Use this way to get Path object
        Path path = Paths.get("foo", "bar", "file.txt");
        Path sunImage = Paths.get("sun.gif");

        // Files class is another new class from java 7 and it includes a lot of useful methods for working with files, writing and reading bytes or characters
        System.out.printf("File %s is readable? %s\n", sunImage, Files.isReadable(sunImage));
        try {
            Path sunCopy = Files.copy(sunImage, Paths.get("sun-copy.gif"));
            System.out.printf("Have just created the copy of the sun.gif: %s\n", sunCopy.toString());
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Path directory = Files.createDirectory(Paths.get("my"));

            System.out.printf("Have just created the directory %s\n", directory);

            Path myFile = Files.createFile(directory.resolve("myFile.txt"));

            System.out.printf("Have just created the file %s (%s)\n", myFile, myFile.toAbsolutePath());

            System.out.printf("Is %s a directory? %s\n", myFile, Files.isDirectory(myFile));
        } catch (IOException e) {
            e.printStackTrace();
        }

    }
}
