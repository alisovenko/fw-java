package com.fitzwilliam.lecture12.others;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;

/**
 * @author alisovenko
 *         11/23/16.
 */
public class OuputStreamPracticeWithAutoClose {
    public static void main(String[] args) throws IOException {
        try (OutputStream ous = Files.newOutputStream(Paths.get("some.file"), StandardOpenOption.CREATE);
             BufferedOutputStream bout = new BufferedOutputStream(ous)) {
            byte[] bytes = new byte[]{0x23, 0x63, 0x01, 0x44};
            bout.write(bytes);
            bout.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
