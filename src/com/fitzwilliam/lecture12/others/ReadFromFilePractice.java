package com.fitzwilliam.lecture12.others;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.List;

/**
 * @author alisovenko
 *         11/24/16.
 */
public class ReadFromFilePractice {
    public static void main(String[] args) {
        try {
            List<String> lines = Files.readAllLines(Paths.get("test.txt"));

            for (String line : lines) {
                System.out.println(line);
            }
        } catch (IOException e) {
            System.out.println("Failed to read file 'test.txt'");
            e.printStackTrace();
        }
    }
}
