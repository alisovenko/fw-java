package com.fitzwilliam.lecture12.fig14_07_09;// Fig. 14.7: CreateTextFile.java
// Writing data to a text file with class Formatter.

import java.io.FileNotFoundException;
import java.util.Formatter;
import java.util.FormatterClosedException;
import java.util.NoSuchElementException;
import java.util.Scanner;

public class CreateTextFile {
    private Formatter output; // object used to output text to file

    // enable user to open file
    public void openFile() {
        try {
            output = new Formatter("clients.txt");
        } // end try
        catch (SecurityException securityException) {
            System.err.println(
                    "You do not have write access to this file.");
            System.exit(1);
        } // end catch
        catch (FileNotFoundException filesNotFoundException) {
            System.err.println("Error creating file.");
            System.exit(1);
        } // end catch
    } // end method openFile

    // add records to file
    public void addRecords() {
        // object to be written to file
        AccountRecord record = new AccountRecord();

        Scanner input = new Scanner(System.in);

        System.out.printf("%s\n%s\n%s\n%s\n\n",
                "To terminate input, type the end-of-file indicator ",
                "when you are prompted to enter input.",
                "On UNIXLinuxMac OS X type <ctrl> d then press Enter",
                "On Windows type <ctrl> z then press Enter");

        System.out.printf("%s\n%s",
                "Enter account number (> 0), first name, last name and balance.",
                "? ");

        while (input.hasNext()) // loop until end-of-file indicator
        {
            try // output values to file
            {
                // retrieve data to be output
                record.setAccount(input.nextInt()); // read account number
                record.setFirstName(input.next()); // read first name
                record.setLastName(input.next()); // read last name
                record.setBalance(input.nextDouble()); // read balance

                if (record.getAccount() > 0) {
                    // write new record
                    output.format("%d %s %s %.2f\n", record.getAccount(),
                            record.getFirstName(), record.getLastName(),
                            record.getBalance());
                } // end if
                else {
                    System.out.println(
                            "Account number must be greater than 0.");
                } // end else
            } // end try
            catch (FormatterClosedException formatterClosedException) {
                System.err.println("Error writing to file.");
                return;
            } // end catch
            catch (NoSuchElementException elementException) {
                System.err.println("Invalid input. Please try again.");
                input.nextLine(); // discard input so user can try again
            } // end catch

            System.out.printf("%s %s\n%s", "Enter account number (>0),",
                    "first name, last name and balance.", "? ");
        } // end while
    } // end method addRecords

    // close file
    public void closeFile() {
        if (output != null)
            output.close();
    } // end method closeFile
} // end class CreateTextFile

class AccountRecord {
    private int account;
    private String firstName;
    private String lastName;
    private double balance;

    // no-argument constructor calls other constructor with default values
    public AccountRecord() {
        this(0, "", "", 0.0); // call four-argument constructor
    } // end no-argument AccountRecord constructor

    // initialize a record
    public AccountRecord(int acct, String first, String last, double bal) {
        setAccount(acct);
        setFirstName(first);
        setLastName(last);
        setBalance(bal);
    } // end four-argument AccountRecord constructor

    // set account number
    public void setAccount(int acct) {
        account = acct;
    } // end method setAccount

    // get account number
    public int getAccount() {
        return account;
    } // end method getAccount

    // set first name
    public void setFirstName(String first) {
        firstName = first;
    } // end method setFirstName

    // get first name
    public String getFirstName() {
        return firstName;
    } // end method getFirstName

    // set last name
    public void setLastName(String last) {
        lastName = last;
    } // end method setLastName

    // get last name
    public String getLastName() {
        return lastName;
    } // end method getLastName

    // set balance
    public void setBalance(double bal) {
        balance = bal;
    } // end method setBalance

    // get balance
    public double getBalance() {
        return balance;
    } // end method getBalance
} // end class AccountRecord

