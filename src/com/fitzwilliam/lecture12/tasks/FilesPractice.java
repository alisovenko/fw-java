package com.fitzwilliam.lecture12.tasks;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.nio.file.StandardCopyOption;

/**
 * @author alisovenko
 *         11/23/16.
 */
public class FilesPractice {
    public static void main(String[] args) {
        // Path class is new class from java 7 and allows to work flexibly with any paths (subpath, merge, resolve, get i member)
        // Use this way to get Path object
        Path path = Paths.get("foo", "bar", "file.txt");
        Path sunImage = Paths.get("sun.gif");

        // Files class is another new class from java 7 and it includes a lot of useful methods for working with files, writing and reading bytes or characters
        System.out.printf("File %s is readable? %s\n", sunImage, Files.isReadable(sunImage));
        try {
            Path sunCopy = Files.copy(sunImage, Paths.get("sun-copy.gif"), StandardCopyOption.REPLACE_EXISTING);
            System.out.printf("Have just created the copy of the sun.gif: %s\n", sunCopy.toString());
            Files.delete(sunCopy);
        } catch (IOException e) {
            e.printStackTrace();
        }

        try {
            Path directory = Files.createDirectories(Paths.get("my", "another", "dir"));

            System.out.printf("Have just created the directory %s\n", directory);

            Path myFile = directory.resolve("myFile.txt");
            if (!Files.exists(myFile)) {
                myFile = Files.createFile(myFile);
            }

            System.out.printf("Have just created the file %s (%s)\n", myFile, myFile.toAbsolutePath());

            System.out.printf("Is %s a directory? %s\n", myFile, Files.isDirectory(myFile));

            System.out.println(Files.getLastModifiedTime(myFile));
            System.out.println(Files.getOwner(myFile));
            System.out.println(Files.getPosixFilePermissions(myFile));

            deleteRecursively(directory.getParent().getParent());
        } catch (IOException e) {
            e.printStackTrace();
        }

    }

    private static void deleteRecursively(Path file) throws IOException {
        if (Files.isDirectory(file))
            Files.list(file).forEach(f -> {
                try {
                    deleteRecursively(f);
                } catch (IOException e) {
                    e.printStackTrace();
                }
            });
        Files.delete(file);
        System.out.println("deleted " + file);
    }
}
