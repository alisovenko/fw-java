package com.fitzwilliam.lecture12.tasks;

import java.io.BufferedOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.nio.file.StandardOpenOption;
import java.util.Arrays;

/**
 * @author alisovenko
 *         11/23/16.
 */
public class OuputStreamPracticeWithAutoClose {
    public static void main(String[] args) throws IOException {
        try (OutputStream ous = Files.newOutputStream(Paths.get("some.file"), StandardOpenOption.CREATE);
             BufferedOutputStream bout = new BufferedOutputStream(ous)) {
            byte[] bytes = new byte[]{0x23, 0x63, 0x01, 0x44};
            bout.write(bytes);
            bout.flush();

            byte[] bytes2 = Files.readAllBytes(Paths.get("some.file"));

            if (bytes.length != bytes2.length)
                throw new IllegalStateException("WFT? Why did this happen?");

            if (Arrays.equals(bytes, bytes2))
                System.out.println("Equal");
            else
                System.out.println("Not equal");

        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
