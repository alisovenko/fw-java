package com.fitzwilliam.lecture13.fig24_09_12;// Fig. 24.10: ServerTest.java
// Tests the Server class.

import javax.swing.*;

public class ServerTest {
    public static void main(String args[]) {
        Server application = new Server(); // create server
        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        application.waitForPackets(); // run server application
    } // end main
} // end class ServerTest

