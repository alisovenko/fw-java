package com.fitzwilliam.lecture13.fig24_09_12;// Fig. 24.12: ClientTest.java
// Tests the Client class.

import javax.swing.*;

public class ClientTest {
    public static void main(String args[]) {
        Client application = new Client(); // create client
        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        application.waitForPackets(); // run client application
    } // end main
}  // end class ClientTest


