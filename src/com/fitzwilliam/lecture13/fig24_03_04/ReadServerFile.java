package com.fitzwilliam.lecture13.fig24_03_04;// Fig. 24.3: ReadServerFile.java
// Use a JEditorPane to display the contents of a file on a web server.

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;

public class ReadServerFile extends JFrame {
    private JTextField enterField; // JTextField to enter site name
    private JEditorPane contentsArea; // to display website

    // set up GUI
    public ReadServerFile() {
        super("Simple Web Browser");

        // create enterField and register its listener
        enterField = new JTextField("Enter file URL here");
        enterField.addActionListener(
                new ActionListener() {
                    // get document specified by user
                    public void actionPerformed(ActionEvent event) {
                        getThePage(event.getActionCommand());
                    } // end method actionPerformed
                } // end inner class
        ); // end call to addActionListener

        add(enterField, BorderLayout.NORTH);

        contentsArea = new JEditorPane(); // create contentsArea
        contentsArea.setEditable(false);
        contentsArea.addHyperlinkListener(
                new HyperlinkListener() {
                    // if user clicked hyperlink, go to specified page
                    public void hyperlinkUpdate(HyperlinkEvent event) {
                        if (event.getEventType() ==
                                HyperlinkEvent.EventType.ACTIVATED)
                            getThePage(event.getURL().toString());
                    } // end method hyperlinkUpdate
                } // end inner class
        ); // end call to addHyperlinkListener

        add(new JScrollPane(contentsArea), BorderLayout.CENTER);
        setSize(400, 300); // set size of window
        setVisible(true); // show window
    } // end ReadServerFile constructor

    // load document
    private void getThePage(String location) {
        try // load document and display location
        {
            contentsArea.setPage(location); // set the page
            enterField.setText(location); // set the text
        } // end try
        catch (IOException ioException) {
            JOptionPane.showMessageDialog(this,
                    "Error retrieving specified URL", "Bad URL",
                    JOptionPane.ERROR_MESSAGE);
        } // end catch
    } // end method getThePage
} // end class ReadServerFile


