package com.fitzwilliam.lecture13.fig24_03_04;// Fig. 24.4: ReadServerFileTest.java
// Create and start a ReadServerFile.

import javax.swing.*;

public class ReadServerFileTest {
    public static void main(String args[]) {
        ReadServerFile application = new ReadServerFile();
        application.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    } // end main
} // end class ReadServerFileTest


