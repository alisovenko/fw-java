package com.fitzwilliam.lecture13.simple;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;

/**
 * @author alisovenko
 *         11/28/16.
 */
public class SimpleServer {
    public static void main(String[] args) throws IOException {
        String portStr = JOptionPane.showInputDialog("Enter locahost port where server will be run:");
        int port = Integer.parseInt(portStr);
        try (ServerSocket listener = new ServerSocket(port)) {
            while (true) {
                System.out.println("Server is running on port " + port + "!!!");

                try (Socket socket = listener.accept()) {
                    PrintWriter out = new PrintWriter(socket.getOutputStream(), true);
                    BufferedReader in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
                    while (true) {
                        String input = in.readLine();
                        if (input == null || input.equals(".")) {
                            break;
                        }
                        out.println("Answer from server: " + input);
                    }
                }
            }
        }
    }
}
