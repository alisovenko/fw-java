package com.fitzwilliam.lecture13.simple;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

/**
 * @author alisovenko
 *         11/28/16.
 */
public class SimpleClient {
    public static void main(String[] args) throws IOException {
        String portStr = JOptionPane.showInputDialog("Enter port of a machine that is\nrunning the date service:");
        int port = Integer.parseInt(portStr);
        Socket s = new Socket("localhost", port);
        BufferedReader input = new BufferedReader(new InputStreamReader(s.getInputStream()));
        PrintWriter out = new PrintWriter(s.getOutputStream(), true);
        while (true) {
            String msg = JOptionPane.showInputDialog("Enter the message to the server");
            out.println(msg);
            String response = input.readLine();
            if (response == null || response.equals("")) {
                System.exit(0);
            }
            JOptionPane.showMessageDialog(null, response);
        }
    }
}
