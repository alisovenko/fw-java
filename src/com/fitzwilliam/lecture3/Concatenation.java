package com.fitzwilliam.lecture3;

/**
 * @author alisovenko
 *         10/25/16.
 */
public class Concatenation {
    public static void main(String[] args) {
        String s = "Hi ";
        int p = 5;

        String x = s + p;
//        int f = p + s;

        System.out.println(s + p);
        System.out.println(p + s);
    }
}
