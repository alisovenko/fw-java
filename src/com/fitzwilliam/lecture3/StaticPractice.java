package com.fitzwilliam.lecture3;

/**
 * @author alisovenko
 *         10/25/16.
 */
public class StaticPractice {
    private int count;
    private static int staticCount;
    private static final String CONST = "id of some guy";
    private final int p;

    public StaticPractice() {
        p = 5;
    }

    public void doSomething() {
        System.out.println("Called from object");
        count++;
        staticCount++;
        System.out.println(CONST);
    }

    public static void doStatic() {
        System.out.println("Called from class");
//        count++;
        staticCount++;
    }

    public static void main(String[] args) {
        StaticPractice staticPractice1 = new StaticPractice();
        StaticPractice staticPractice2 = new StaticPractice();

        StaticPractice.staticCount++;
        staticCount++;
        staticCount++;

        staticPractice1.count++;
        staticPractice2.count++;

        System.out.println(staticCount);
        System.out.println(staticPractice1.count);
        System.out.println(staticPractice2.count);

        //*****************

        doStatic();
        StaticPractice.doStatic();

        new StaticPractice().doSomething();

        Math.abs(4);
    }
}
