package com.fitzwilliam.lecture3;

import java.util.Arrays;

/**
 * @author alisovenko
 *         10/25/16.
 */
public class EnumPractice {
    public static void main(String[] args) {
        System.out.println(Arrays.toString(State.values()));
        State s = State.STOPPED;
        String m = "message";

        if (m.equals("message")) {

        }

        if (s == State.STARTED) {
            System.out.println("Something");
        }
    }
}
