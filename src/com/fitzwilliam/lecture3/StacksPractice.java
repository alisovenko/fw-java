package com.fitzwilliam.lecture3;

/**
 * @author alisovenko
 *         10/25/16.
 */
public class StacksPractice {
    private enum Some {
        True, False;
    }

    public void method1() {
        int i = 2;
        method2();
    }

    private void method2() {
        int p = 5;
        method3();
    }

    private void method3() {
        String v = "Hi";
        System.out.println();
    }

    public static void main(String[] args) {
        new StacksPractice().method1();
    }
}
