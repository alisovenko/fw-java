package com.fitzwilliam.lecture3;

/**
 * @author alisovenko
 *         10/25/16.
 */
public enum State {
    STARTED("started"), STOPPED("stopped"), SUSPENDED("suspended");

    final String message;

    State(String message) {
        this.message = message;
    }
}
