package com.fitzwilliam.lecture1;

/**
 * Created by alisovenko on 10/18/16.
 */
public class GradeBook {
    private String courseName;

    public void setCourseName(String name) {
        this.courseName = name;
    }

    public String getCourseName() {
        return courseName;
    }

    public void displayMessage() {
//        System.out.printf("Our course is: %s\n", courseName);
        System.out.println("Our course is: " + courseName + ". Yeah!");
    }
}
