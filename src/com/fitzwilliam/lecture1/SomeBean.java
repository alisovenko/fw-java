package com.fitzwilliam.lecture1;

/**
 * @author alisovenko
 *         10/18/16.
 */
public class SomeBean {
    private int foo;
    private String bar;

    public void setFoo(int foo) {
        this.foo = foo;
    }

    public int getFoo() {
        return foo;
    }

    public String getBar() {
        return bar;
    }

    public void setBar(String bar) {
        this.bar = bar;
    }
}
