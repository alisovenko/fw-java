package com.fitzwilliam.lecture1;

/**
 * @author alisovenko
 *         10/18/16.
 */
public class FitzwilliamGrade {
    private String courseName;
    private int year;

    public FitzwilliamGrade(String courseName, int year) {
        this.courseName = courseName;
        this.year = year;
    }

    public FitzwilliamGrade(String courseName, double some) {
        this(courseName, (int) some);
    }

    public void displayName(String name) {
        int y = 2;
        double p = 0.1;
        float f = 0.6f;
        long x = 3432423423423432L;
//        System.out.println(y);
        System.out.println(year);
        System.out.println(name);
    }

    public void foo() {
        year = 30;
    }

    private void bar() {

    }
}
