package com.fitzwilliam.lecture1;

/**
 * @author alisovenko
 *         10/20/16.
 */
public class Account {
    private double balance;

    public Account(double balance) {
        this.balance = balance;
    }

    public void credit(float amount) {
        balance = balance + amount;
    }

    public double getBalance() {
        return balance;
    }

    public static void main(String[] args) {
        Account acc = new Account(0.1);
//        acc.credit(34324234.0);
        System.out.println(acc.getBalance());
    }
}
