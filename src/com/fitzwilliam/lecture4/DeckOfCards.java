package com.fitzwilliam.lecture4;

import java.util.Random;

/**
 * @author alisovenko
 *         10/24/16.
 */
public class DeckOfCards {
    private Card[] deck;
    private int currentCard = 0;
    private final int NUMBER_OF_CARDS = 52;
    private Random random = new Random();

    public DeckOfCards() {
        String[] faces = {"Ace", "King", "Queen", "Jack", "Ten", "Nine", "Eight", "Seven", "Six", "Five", "Four", "Three", "Two"};
        String suits[] = {"Hearts", "Clubs", "Spades", "Diamonds"};

        deck = new Card[NUMBER_OF_CARDS];

        for (int i = 0; i < deck.length; i++) {
            deck[i] = new Card(faces[i % 13], suits[i / 13]);
        }
    }

    public void shuffle() {
        currentCard = 0;

        for (int i = 1; i < deck.length; i++) {
            int second = random.nextInt(i + 1);

            // swap
            Card t = deck[i];
            deck[i] = deck[second];
            deck[second] = t;
        }
    }

    public Card dealCard() {
        if (currentCard < deck.length)
            return deck[currentCard++];
        else
            return null;
    }
}
