package com.fitzwilliam.lecture4;

/**
 * @author alisovenko
 *         10/25/16.
 */
public class GradeBookMultidimensional {
    private String courseName;
    private int[][] grades;

    public GradeBookMultidimensional(String courseName, int[][] grades) {
        this.courseName = courseName;
        this.grades = grades;
    }

    public void displayMessage() {
        System.out.printf("Welcome to the grade book for \n%s!\n\n", courseName);
    }

    public void processGrades() {
        outputGrades();

        System.out.printf("\nLowest grade is %d\nHighest grade is %d\n\n", getMinimum(), getMaximum());

        outputBarChart();
    }

    private int getMaximum() {
        int highGrade = grades[0][0];

        for (int row = 0; row < grades.length; row++) {
            for (int col = 0; col < grades[row].length; col++) {
                if (highGrade < grades[row][col])
                    highGrade = grades[row][col];
            }
        }
        return highGrade;
    }

    private int getMinimum() {
        int lowGrade = grades[0][0];

        for (int[] gradeRow : grades) {
            for (int grade : gradeRow) {
                if (lowGrade > grade)
                    lowGrade = grade;
            }
        }
        return lowGrade;
    }

    private double getAverage(int setOfGrades[]) {
        int total = 0;
        for (int grade : setOfGrades) {
            total += grade;
        }
        return (double) total / setOfGrades.length;
    }

    private void outputBarChart() {
        System.out.println("Grade distribution:");

        int[] frequency = new int[11];

        for (int[] gradesRow : grades) {
            for (int grade : gradesRow) {
                ++frequency[grade / 10];
            }
        }

        for (int count = 0; count < frequency.length; count++) {
            if (count == 10)
                System.out.printf("%5d: ", 100);
            else
                System.out.printf("%02d-%02d: ", count * 10, count * 10 + 9);

            for (int stars = 0; stars < frequency[count]; stars++) {
                System.out.print("*");
            }
            System.out.println();
        }
    }

    private void outputGrades() {
        System.out.println("The grades are:\n");

        System.out.print("           ");

        for (int i = 0; i < grades[0].length; i++)
            System.out.printf("Test %d  ", i + 1);

        System.out.println("Average");

        for (int i = 0; i < grades.length; i++) {
            System.out.printf("Student%2d", i + 1);

            for (int grade : grades[i])
                System.out.printf("%8d", grade);

            double average = getAverage(grades[i]);
            System.out.printf("%9.2f\n", average);
        }
    }

    public static void main(String[] args) {
        int grades[][] = {{94, 87, 67}, {40, 79, 84}, {98, 99, 100}};
        GradeBookMultidimensional gradeBook = new GradeBookMultidimensional("Java Course", grades);

        gradeBook.displayMessage();
        gradeBook.processGrades();
    }

}
