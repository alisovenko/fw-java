package com.fitzwilliam.lecture4;

/**
 * @author alisovenko
 *         10/25/16.
 */
public class GradeBook {
    private String courseName;
    // TODO add array variable here

    public GradeBook(String courseName) {
        this.courseName = courseName;
    }

    public void displayMessage() {
        System.out.printf("Welcome to the grade book for \n%s!\n\n", courseName);
    }

    public void processGrades() {
        outputGrades();

        System.out.printf("\nClass average is %.2f\n", getAverage());
        System.out.printf("\nLowest grade is %d\nHighest grade is %d\n\n", getMinimum(), getMaximum());

        outputBarChart();
    }

    private void outputBarChart() {
        System.out.println("Grade distribution:");

        // todo with teacher
    }

    private int getMaximum() {
        // todo with teacher
        return 0;
    }

    private int getMinimum() {
        // todo yourself: use getMaximum() procedure but change the loop to enhanced style
        return 0;
    }

    private double getAverage() {
        // todo yourself
        return 0;
    }

    private void outputGrades() {
        System.out.println("The grades are:\n");

        /*for (int i = 0; i < grades.length; i++) {
            System.out.printf("Student %2d: %3d\n", i + 1, grades[i]);
        }*/
    }

    public static void main(String[] args) {
        //int grades[] = ; todo
        GradeBook gradeBook = new GradeBook("Java Course");

        gradeBook.displayMessage();
        gradeBook.processGrades();
    }

}
