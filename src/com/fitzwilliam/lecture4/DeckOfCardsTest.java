package com.fitzwilliam.lecture4;

/**
 * @author alisovenko
 *         10/24/16.
 */
public class DeckOfCardsTest {
    public static void main(String[] args) {
        DeckOfCards deckOfCards = new DeckOfCards();
        deckOfCards.shuffle();

        for (int i = 0; i < 13; i++) {
            System.out.printf("%-20s%-20s%-20s%-20s\n", deckOfCards.dealCard(), deckOfCards.dealCard(), deckOfCards.dealCard(), deckOfCards.dealCard());
        }
    }
}
