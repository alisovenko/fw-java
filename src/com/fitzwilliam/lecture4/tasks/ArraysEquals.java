package com.fitzwilliam.lecture4.tasks;

/**
 * @author alisovenko
 *         11/2/16.
 */
public class ArraysEquals {
    public static boolean equals(int[] array1, int[] array2) {
        // 1. Check for null parameters
        // if (object == null) or if (object != null)
        // what if both of arguments are null? Only one of them?

        // 2. Check for sizes of arrays. If their sizes are not equal - what does it mean?

        // 3. Compare elements on the same positions for each element. Use "fori" loop.

        return true;
    }

    public static void main(String[] args) {
        // null array is not equal to another null
        checkNotEquals(null, null);

        // null array is not equal to non-null
        checkNotEquals(null, new int[0]);
        checkNotEquals(new int[0], null);

        // arrays of different sizes are not equal
        checkNotEquals(new int[0], new int[1]);

        // arrays of the same size but different elements are not equal
        checkNotEquals(new int[]{1, 2, 5}, new int[]{1, 2, 3});

        // equal arrays
        checkEquals(new int[]{6, 5, 4}, new int[]{6, 5, 4});
    }

    private static void checkEquals(int[] a1, int[] a2) {
        boolean result = equals(a1, a2);
        if (result)
            System.out.println("Correct");
        else
            System.out.println("Wrong!");
    }

    private static void checkNotEquals(int[] a1, int[] a2) {
        boolean result = equals(a1, a2);
        if (result)
            System.out.println("Wrong!");
        else
            System.out.println("Correct");
    }
}
