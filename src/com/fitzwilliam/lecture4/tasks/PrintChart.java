package com.fitzwilliam.lecture4.tasks;

/**
 * @author alisovenko
 *         11/2/16.
 */
public class PrintChart {
    private static int[] readInput() {
        // Read input number, create the array of this size
        // then read new numbers for array in a loop and initialize matching elements in the array
        return null;
    }

    private static void printChart(int[] arr) {
        // Use two loops: the outer one - through the array parameter, the inner one - from 0 to arr[i] (value of the array) and print '*' there
    }

    public static void main(String[] args) {
        int[] userArray = readInput();

        printChart(userArray);
    }
}
