package com.fitzwilliam.lecture4;

/**
 * @author alisovenko
 *         10/27/16.
 */
public class ArrayPractice {
    public static void main(String[] args) {
        int[] arr = createAndThenInitialize();
        int[] arr2 = createAtOneLine();

        printArray(arr);
        printArray(arr2);
        printArray2(arr);
        printArray(new int[]{2, 5, 6, 7});
    }

    private static void printArray2(int[] arr) {
        for (int element : arr) {
            System.out.println(element);
        }
        for (int i : arr) {
            System.out.println(i);
        }
    }

    private static void printArray(int[] arr) {
        for (int counter = 0; counter < arr.length; counter++) {
            System.out.printf("[%d]: %d\n", counter, arr[counter]);
        }
        System.out.println();
    }

    private static int[] createAtOneLine() {
        int[] a = {1, 6, 2, 6};
        return a;
    }

    private static int[] createAndThenInitialize() {
        int[] a = new int[3];
        a[0] = 1;
        a[1] = 3;
        a[2] = 5;

        return a;
    }
}
