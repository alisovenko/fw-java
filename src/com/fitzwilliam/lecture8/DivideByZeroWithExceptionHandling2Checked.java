//By Fitzwilliam Institute
package com.fitzwilliam.lecture8;

import java.io.IOException;
import java.util.InputMismatchException;
import java.util.Scanner;

public class DivideByZeroWithExceptionHandling2Checked {
    // demonstrates throwing an exception when a divide-by-zero occurs
    public static int quotient(int numerator, int denominator)
            throws ArithmeticException {
        return numerator / denominator; // possible division by zero
    } // end method quotient

    public static void main(String args[]) {
        Scanner scanner = new Scanner(System.in); // scanner for input
        boolean continueLoop = true; // determines if more input is needed

        do {
            try // read two numbers and calculate quotient
            {
                System.out.print("Please enter number that yoy wish to divide: ");
                String numeratorStr = scanner.next();

                int numerator = Integer.parseInt(numeratorStr);

                System.out.print("Please enter an divider: ");
                int denominator = scanner.nextInt();

                int result = quotient(numerator, denominator);
                System.out.printf("\nResult: %d / %d = %d\n", numerator,
                        denominator, result);
                continueLoop = false; // input successful; end looping

                // you must handle this CHECKED exception or mark method as "throws"!
                throw new IOException();
            }
            // will not compile!
            /*catch (Exception e) {
                System.out.println(e);
            }*/ catch (InputMismatchException inputMismatchException) {
                System.err.printf("\nException: %s\n",
                        inputMismatchException);
                scanner.nextLine(); // discard input so user can try again
                System.out.println(
                        "You must enter integers. Please try again.\n");
            } // end catch
            catch (ArithmeticException arithmeticException) {
                System.err.printf("\nException: %s\n", arithmeticException);
                System.out.println(
                        "Zero is an invalid divider. Please try again.\n");
            }
            // Handling of checked exception - if you remove it - class will not compile!
            catch (IOException e) {
                System.out.println(e);
            }
        } while (continueLoop); // end do...while
    } // end main
} // end class DivideByZeroWithExceptionHandling